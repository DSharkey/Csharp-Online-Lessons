# .NET Core Online Lesson Plan

### Lecture 1:
#### Intro to .NET Core
This lecture's goal is to go over the history of .NET Core, it's relation and differences to languages such as Python and Javascript, as well as going into some of the basics of the language itself including geneal variables, types, control structures, and collections.

* C# as a static strongly typed language
* .NET Core as the future of C# and how it differs from past iterations
* Creating and running a .NET Core terminal application
* Variables and Types
* Conditional statements and Loops
* Collection types (Array, List, Dictionary)
* Iterating across a collection
* Creating and calling functions

### Lecture 2:
#### OOP with C#
Go over the basics of why OOP is used again. Aim to build a few classes and objects quickly, but the real goal here is to show polymorphism and the usage of interfaces since these concepts tend to be the most challeneging to grasp for students. This includes method overriding, casting, and showing a practical implementation for a interface.

* OOP whys
* Classes and Objects
* Public/Private/Protected
* Class Inheritence
* Polymorphism and Scope of Class Methods (Method Overriding)
* Interfaces

### Weekend Lecture 1
#### Capping off on C# niches and Advanced OOP
A good time to describe get and set public accessors for variables as well as get into what generics are and how they would be used. Other than that always good to touch on OOP concepts again since they are tricky

* Get and Set public accessors
* Generics
* Abstract classes
* Extension methods
* Delegates

### Lecture 3:
#### LINQ
Discussion on why LINQ is useful and going over the various methods avaliable working with some collection set. This lecture works best as more of a demo then a lecture. It is always useful that after demoing a few methods have the students generate some additional ways you might want to organize and query the collection using LINQ

* LINQ Methods
* IEnumerable Interface

### Lecture 4:
#### MySQL with .NET Core
A simple demo of using the MySql Connector class to connect to a Database with ASP.NET Core. Talk about the IDbConnection interface and ease of connecting to a different db due to it.

* Walkthrough on the MySql Connector class
* Querying a Database
* Handling return Data as a List of Dictionary types

### Weekend Lecture 2:
####

### Lecture 5:
#### Making a Web App with ASP.NET Core

Talk through was ASP.NET Core is and similar to the intor talk how it is different than past ASP.NET versions. For a lot of the content it is easy enough to relate dicussion points back to Flask or Django (easy example is how controller routing is very much like Flask's routing system). Do not get too much into passing data to a view as this is saved for the next lecture. Make simple html pages for now.

* Setting up a base web app using dotnet new web
* Walkthrough on updated Program.cs and new Startup.cs files
* Running the web app
* Moving to MVC structure
* Routing in MVC
* Displaying a string first, then a JSON object
* Rendering a View
* The IActionResult Interface
* Catching POST data & Redirecting

### Lecture 6:
#### Razor View Engine

This lecture is all about sharing data between the view and the backend. Discuss the ins and outs of Razor. Mention how razor view files are built at runtime and as such don't require a server restart for changes. Get into Sessions and TempData to persist data as needed. 

* Making use of ViewBag
* Using C# on a Razor Page
* Session and TempData

### Weekend Lecture 3:
#### AJAX Refresher

Students have limited exposure to jQuery in Python so it helps to have a refresher. It is also important to express how this same process can be repeated to have C# as a functional backend hooks to any frontend such as Angular or React as well.

* How to run a jQuery .Get and .Post
* Using JSON response for your ASP.NET Core app to function as an API

### Lecture 7:
#### Web APIs and handling JSON

The main goal of this lecture is to show to students the problems that arise when handling dynamic data such as JSON with a static language like C#. Prepare to cast some things! This is also a great time to refresh on using the debugger as it makes parsing the recieved JSON a good bit easier. Console.WriteLines will not save you here!

* The ApiCaller class
* Making a url request
* NewtonSoft.Json, JArrays, JObjects
* (Optional) Debugger walkthrough works well here
* Mutating the ApiCaller to fit your needs (accepting a passed url)

### Lecture 8:
#### ASP.NET Core with MySQL

MySQL returns! Showing that this is the same MySql connector file from fundamentals and hitting home that the concepts that have applied there have not changed now. Stress that connecting a database and displaying that recieved data is not that big of a deal in any way.

* Recap on Mysql Connector File
* Usage of this file in a Controller
* Taking results of a query and displaying them with Razor
* Taking post data as input for Database Queries

### Weekend Lecture 4:
#### Deploying to Azure

Azure is expensive, but easy! IIS is Azure's version of Nginx. Also talking about getting a MySql instance up and going deployed (and possibly even having it on Amazon RDS instead?)

* How to get an app up and running on Azure
* Hosting with a MySQL Server as well
* Alternatives to Azure

### Lecture 9:
#### Models

Models is a section that tends to trip students up some, but it is actually nothing new at all for them. Important to stress we now call them Models, but they are nothing more than the Classes and Objects from fundamentals. ASP.NET Core does not care about your Models directory like it does Controllers or Views it simply accepts any objects it can map to.

* Models === Objects
* Method parameter mapping with Models
* Mapping Database data to Models

### Lecture 10:
#### Advanced Razor

Walkthrough of Modelstate and some of the inteseting feature that have not been shown yet in Razor that are there mostly to again help with errors.

* Model Validations
* ModelState
* Model Mapping with Razor
* Using Tag Helpers

### Weekend Lecture 5:
#### ViewModels and Razor Partials

Model binding with Razor is annoying because you can only bind one class so fails for something like Login/Reg on the same page; however, you can show how 2 partials can help fix this problem.

* ViewModels to help with Errors
* Partials to help with mapping multiple models to a single page

### Lecture 11:
#### Intro to Dapper 

Dapper is a micro ORM develeoped by StackExchange (Makers of StackOverflow) to get the speed and control that is offered by raw sql queries along with the ease of use that come from model mapping. We still have to write all out SQL queries, but dapper will just handle this simple mapping of the returned data for us.

* What is Dapper?
* How do we make use of it?
* Factories?
* CRUD with Dapper

### Lecture 12:
#### Dapper Relationships

Dapper is good at mapping data to models, but not great at connecting models to each other. For that we simply need to get the data mapped up to thir respective models then handling the connecting of relationships ourselves.

* Query method extension for Many-to-One side
* QueryMultiple for One-To-Many side

### Weekend Lecture 6:
#### Securing a Connection String

* Explain why it is useful
* Getting appsettings.json connected to code
* Dependency Injection and IoC

### Lecture 13:
#### Entity Framework

Where Dapper is aimed at Speed and allows you to take the reigns, EF is a full fledged ORM that looks to handle all interactions for you. This offers a lot of ease of use, but forces you to play by EF's rules. Also in terms of more specific queries, all sorting or selecting is done in LINQ rather than through the query to the Database.

* What is EF?
* How does EF Compare to Dapper
* CRUD with EF

### Lecture 14:
#### Relationships with EF

Relationships in EF are very simple. Using the .include method EF can match up and map Models depending on relationships. Anything more complected than simple joining though requires some LINQ queries to happen.

* One-To-Many relationships with EF and Many-To-Many
* LINQ and EF
* Gotchas with EF relationships and database field names

### Weekend Lecture 7:
#### Deployment to Amazon EC2
* Walkthrough on the process mainly